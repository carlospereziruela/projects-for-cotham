package com.example.carlos.booklistingapp;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<Book>> {
    public static final String LOG_TAG = MainActivity.class.getName();
    private BookAdapter adapter;
    private static final int BOOK_LOADER_ID = 1;
    private TextView mEmptyStateTextView;
    //AIzaSyA1maMHTssAz-NhuTbpqjIs-s0TjKzYmCI
   // api-key=91b678f6-adbe-4e4f-91d2-0d874b0531b5
    //&api-key=AIzaSyA1maMHTssAz-NhuTbpqjIs-s0TjKzYmCI
    private static String USGS_REQUEST_URL="https://www.googleapis.com/books/v1/volumes?maxResults=10&q=android&api-key=AIzaSyA1maMHTssAz-NhuTbpqjIs-s0TjKzYmCI";

    public void search(View view){
        SearchView search=(SearchView) findViewById(R.id.search);
        String busqueda=search.getQuery().toString();
        USGS_REQUEST_URL="https://www.googleapis.com/books/v1/volumes?maxResults=10&q=".concat(busqueda+"&api-key=AIzaSyA1maMHTssAz-NhuTbpqjIs-s0TjKzYmCI");
        Intent intento = new Intent(this, MainActivity.class);
        startActivity(intento);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //final List<Book> books=extractFeatureFromJson(JSON);
        //Bitmap imagen=BitmapFactory.decodeResource(getResources(),R.drawable.thoreau);
        GridView gridView = (GridView) findViewById(R.id.gridBooks);
        mEmptyStateTextView = (TextView) findViewById(R.id.empty_view);
        gridView.setEmptyView(mEmptyStateTextView);

        adapter = new BookAdapter(this, new ArrayList<Book>());
        //adapter.addAll(books);
        gridView.setAdapter(adapter);


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                // Find the current earthquake that was clicked on
                Book currentBook = adapter.getItem(position);

                // Convert the String URL into a URI object (to pass into the Intent constructor)
                Uri bookUri = Uri.parse(currentBook.getUrl());

                // Create a new intent to view the earthquake URI
                Intent websiteIntent = new Intent(Intent.ACTION_VIEW, bookUri);

                // Send the intent to launch a new activity
                startActivity(websiteIntent);
            }
        });

        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            /*LÍNEA NO PRESCINDIBLE*/LoaderManager loaderManager = getLoaderManager();
            /*LÍNEA NO PRESCINDIBLE*/loaderManager.initLoader(BOOK_LOADER_ID, null, this);
        } else {
            View loadingIndicator = findViewById(R.id.loading_indicator);
            loadingIndicator.setVisibility(View.GONE);
            mEmptyStateTextView.setText("Sin conexxion a internet");
        }
    }

    public Loader<List<Book>> onCreateLoader(int i, Bundle bundle) {
        // Create a new loader for the given URL
        return new BookLoader(this, USGS_REQUEST_URL);
    }
    public void onLoadFinished(Loader<List<Book>> loader, List<Book> books) {
        //LOADING
        View loadingIndicator = findViewById(R.id.loading_indicator);
        loadingIndicator.setVisibility(View.GONE);
        //Terremotos no encontrados
        mEmptyStateTextView.setText("books no encontrados");
        // Clear the adapter of previous earthquake data
        adapter.clear();

        // If there is a valid list of {@link Earthquake}s, then add them to the adapter's
        // data set. This will trigger the ListView to update.
        if (books != null && !books.isEmpty()) {
            adapter.addAll(books);
        }
    }
    public void onLoaderReset(Loader<List<Book>> loader) {
        // Loader reset, so we can clear out our existing data.
        adapter.clear();
    }


    private static List<Book> extractFeatureFromJson(String JSON) {
        if (TextUtils.isEmpty(JSON)) {
            return null;
        }
        List<Book> earthquakesList = new ArrayList<>();
        try {
            JSONObject baseJsonResponse = new JSONObject(JSON);
            JSONArray booksArray = baseJsonResponse.getJSONArray("items");

            for (int i = 0; i < booksArray.length(); i++) {
                JSONObject currentBook  = booksArray.getJSONObject(i);
                JSONObject volumeInfo = currentBook .getJSONObject("volumeInfo");

                String titulo = volumeInfo.getString("title");
                JSONArray authors=volumeInfo.getJSONArray("authors");
                String autor = authors.getString(0);
                String url = volumeInfo.getString("infoLink");
                JSONObject imageLinks=volumeInfo.getJSONObject("imageLinks");
                String imageUrl=imageLinks.getString("smallThumbnail");
                //Bitmap imagen=loadImageFromURL(imageUrl);
                Bitmap imagen = null;
                try {
                    imagen = BitmapFactory.decodeStream((InputStream)new URL("http://books.google.com/books/content?id=9Jo6AwAAQBAJ&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api").getContent());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Book earthquake = new Book(imagen, titulo, autor, url);
                earthquakesList.add(earthquake);
            }
        } catch (JSONException e) {

        }
        return earthquakesList;
    }

    public static Bitmap loadImageFromURL(String url) {
        try {
            //InputStream is = (InputStream) new URL(url).getContent();
            Bitmap bitmap = BitmapFactory.decodeStream((InputStream)new URL(url).getContent());
            //Drawable d = Drawable.createFromStream(is, "src name");
            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }
    /*public static Bitmap loadImageFromURL(String fileUrl){
        try {

            URL myFileUrl = new URL (fileUrl);
            HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
            conn.setDoInput(true);
            conn.connect();

            InputStream is = conn.getInputStream();
            //(ImageView)iv.setImageBitmap(BitmapFactory.decodeStream(is));
            Bitmap imagen=BitmapFactory.decodeStream(is);

            return imagen;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }*/
}
