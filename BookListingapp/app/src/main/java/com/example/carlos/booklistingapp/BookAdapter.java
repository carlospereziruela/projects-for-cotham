package com.example.carlos.booklistingapp;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class BookAdapter extends ArrayAdapter<Book>{
    //private int idBook;

    public BookAdapter(Activity context, ArrayList<Book> books) {
        super(context,0, books);
        //this.idBook=idBook;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.book_view, parent, false);
        }
        Book currentWord = getItem(position);

        ImageView image=(ImageView) convertView.findViewById(R.id.imagen);
        image.setImageBitmap(currentWord.getIdImage());

        TextView tituloView = (TextView) convertView.findViewById(R.id.titulo);
        tituloView.setText(currentWord.getTitulo());

        TextView autorView = (TextView) convertView.findViewById(R.id.autor);
        autorView.setText(currentWord.getAutor());

        return convertView;
    }
}
