package com.example.carlos.booklistingapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Carlos on 27/03/2018.
 */

public class QueryUtils {


    public static final String LOG_TAG = QueryUtils.class.getSimpleName();

    //Este método es el que vincula el resto de métodos por eso es público, para poder acceder desde fuera
    public static List<Book> fetchEarthquakeData(String requestUrl) {
        URL url = createUrl(requestUrl);
        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem making the HTTP request.", e);
        }
        List<Book> booksList = extractFeatureFromJson(jsonResponse);

        return booksList;
    }

    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error with creating URL ", e);
        }
        return url;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        if (url == null) {
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving the earthquake JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    private static List<Book> extractFeatureFromJson(String JSON) {
        if (TextUtils.isEmpty(JSON)) {
            return null;
        }
        List<Book> earthquakesList = new ArrayList<>();
        try {
            System.out.println("A 000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");

            JSONObject baseJsonResponse = new JSONObject(JSON);
            JSONArray booksArray = baseJsonResponse.getJSONArray("items");

            for (int i = 0; i < booksArray.length(); i++) {
                JSONObject currentBook  = booksArray.getJSONObject(i);
                JSONObject volumeInfo = currentBook .getJSONObject("volumeInfo");

                String titulo = volumeInfo.getString("title");
                JSONArray authors=volumeInfo.getJSONArray("authors");
                String autor = authors.getString(0);
                String url = volumeInfo.getString("infoLink");
                JSONObject imageLinks=volumeInfo.getJSONObject("imageLinks");
                String imageUrl=imageLinks.getString("smallThumbnail");

                //Bitmap imagen=loadImageFromURL(imageUrl);
                Bitmap imagen = null;
                try {
                    imagen = BitmapFactory.decodeStream((InputStream)new URL(imageUrl).getContent());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Book earthquake = new Book(imagen, titulo, autor, url);
                earthquakesList.add(earthquake);
            }
        } catch (JSONException e) {

        }
        return earthquakesList;
    }
}
