package com.example.carlos.booklistingapp;

import android.graphics.Bitmap;

public class Book {
    private Bitmap idImage;
    private String titulo;
    private String autor;
    private String url;

    public Book(Bitmap idImage, String titulo, String autor, String url) {
        this.idImage = idImage;
        this.titulo = titulo;
        this.autor = autor;
        this.url=url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Bitmap getIdImage() {
        return idImage;
    }

    public void setIdImage(Bitmap idImage) {
        this.idImage = idImage;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }
}
