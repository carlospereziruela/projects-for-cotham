package com.example.carlos.notesapp;

import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.carlos.notesapp.Data.NotesCointract.NotesEntry;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class EditingMode extends AppCompatActivity  implements LoaderManager.LoaderCallbacks<Cursor>{
    private EditText mTitleEditText;
    private EditText mBodyEditText;
    private TextView mDateEditText;
    private Uri mCurrentNoteUri;
    private static final int EXISTING_NOTE_LOADER = 0;
    private boolean mPetHasChanged = false;
    private SimpleDateFormat dateFormat;
    Date date;
    private String dateInTheMoment(){
        dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss", Locale.getDefault());
        date = new Date();
        String fecha = dateFormat.format(date);
        return fecha;
    }
    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            mPetHasChanged = true;
            return false;
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        savePet();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editing_mode);
        Intent intent=getIntent();

        mTitleEditText = (EditText) findViewById(R.id.main);
        mBodyEditText = (EditText) findViewById(R.id.body);
        mDateEditText = (TextView) findViewById(R.id.date);

        mCurrentNoteUri=intent.getData();
        if(mCurrentNoteUri==null){
            setTitle("New Note");
            mDateEditText.setText(dateInTheMoment());
            //invalidateOptionsMenu();
        }else{
            getLoaderManager().initLoader(EXISTING_NOTE_LOADER, null, this);
        }

        // Find all relevant views that we will need to read user input from

        //Llamadas al listener de cambios
        mTitleEditText.setOnTouchListener(mTouchListener);
        mBodyEditText.setOnTouchListener(mTouchListener);
        mDateEditText.setOnTouchListener(mTouchListener);
    }

    private void savePet(){
        mDateEditText.setText(dateInTheMoment());
        String titleString = mTitleEditText.getText().toString();
        String bodyString = mBodyEditText.getText().toString();
        String dateString = mDateEditText.getText().toString();
        if (mCurrentNoteUri == null &&
                TextUtils.isEmpty(titleString) && TextUtils.isEmpty(bodyString) && TextUtils.isEmpty(dateString)) {
            return;
        }
        ContentValues values = new ContentValues();
        values.put(NotesEntry.COLUMN_NOTES_TITLE, titleString);
        values.put(NotesEntry.COLUMN_NOTES_BODY, bodyString);
        values.put(NotesEntry.COLUMN_NOTES_DATE, dateString);

        if (mCurrentNoteUri == null) {
            Uri newUri = getContentResolver().insert(NotesEntry.CONTENT_URI, values);
            if (newUri == null) {
                Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Successful", Toast.LENGTH_SHORT).show();
            }
        } else {
            int rowsAffected = getContentResolver().update(mCurrentNoteUri, values, null, null);

            // Show a toast message depending on whether or not the update was successful.
            if (rowsAffected == 0) {
                // If no rows were affected, then there was an error with the update.
                Toast.makeText(this, "Failed",
                        Toast.LENGTH_SHORT).show();
            } else {
                // Otherwise, the update was successful and we can display a toast.
                Toast.makeText(this, "Successful", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void deletePet() {
        if (mCurrentNoteUri != null) {
            int rowsDeleted = getContentResolver().delete(mCurrentNoteUri, null, null);
            if (rowsDeleted == 0) {
                Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Successful", Toast.LENGTH_SHORT).show();
            }
        }
        finish();
    }
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = {
                NotesEntry._ID,
                NotesEntry.COLUMN_NOTES_TITLE,
                NotesEntry.COLUMN_NOTES_BODY,
                NotesEntry.COLUMN_NOTES_DATE };

        return new CursorLoader(this, mCurrentNoteUri, projection, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor.moveToFirst()) {
            // Find the columns of pet attributes that we're interested in
            int titleColumnIndex = cursor.getColumnIndex(NotesEntry.COLUMN_NOTES_TITLE);
            int bodyColumnIndex = cursor.getColumnIndex(NotesEntry.COLUMN_NOTES_BODY);
            int dateColumnIndex = cursor.getColumnIndex(NotesEntry.COLUMN_NOTES_DATE);

            // Extract out the value from the Cursor for the given column index
            String title = cursor.getString(titleColumnIndex);
            String body = cursor.getString(bodyColumnIndex);
            String date = cursor.getString(dateColumnIndex);
            setTitle("Editing: " + title);


            mTitleEditText.setText(title);
            mBodyEditText.setText(body);
            mDateEditText.setText(date);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mTitleEditText.setText("");
        mBodyEditText.setText("");
        mDateEditText.setText("");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.delete_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_delete) {
            deletePet();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
