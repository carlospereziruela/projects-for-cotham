package com.example.carlos.notesapp;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.carlos.notesapp.Data.NotesCointract.NotesEntry;

public class NotesCursorAdapter extends CursorAdapter{
    public NotesCursorAdapter(Context context, Cursor c) {
        super(context, c, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.notes_view, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView titleText = (TextView) view.findViewById(R.id.main);
        TextView bodyText = (TextView) view.findViewById(R.id.body);
        TextView dateText = (TextView) view.findViewById(R.id.date);
        // Extract properties from cursor
        String title = cursor.getString(cursor.getColumnIndexOrThrow(NotesEntry.COLUMN_NOTES_TITLE));
        String body = cursor.getString(cursor.getColumnIndexOrThrow(NotesEntry.COLUMN_NOTES_BODY));
        String date = cursor.getString(cursor.getColumnIndexOrThrow(NotesEntry.COLUMN_NOTES_DATE));

        titleText.setText(title);
        bodyText.setText(String.valueOf(body));
        dateText.setText(date);

    }
}
