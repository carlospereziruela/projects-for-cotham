package com.example.carlos.notesapp.Data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public final class NotesCointract{
    public static final String CONTENT_AUTHORITY = "com.example.carlos.notesapp";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_NOTES = "notesapp";

    public static final class NotesEntry implements BaseColumns {
        //Creacion de la uri
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_NOTES);
        //Variables constantes
        public static final String TABLE_NAME="notes";

        public static final String ID=BaseColumns._ID;
        public static final String COLUMN_NOTES_TITLE="title";
        public static final String COLUMN_NOTES_BODY = "body";
        public static final String COLUMN_NOTES_DATE ="date";

        //lo que crea la tabla
        public static final String SQL_CREATE_NOTES_TABLET= "CREATE TABLE " + NotesEntry.TABLE_NAME + " ("
                + NotesEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + NotesEntry.COLUMN_NOTES_TITLE + " TEXT NOT NULL,"
                + NotesEntry.COLUMN_NOTES_BODY + " TEXT,"
                + NotesEntry.COLUMN_NOTES_DATE + " TEXT NOT NULL);";
        //Los MIME de uno y varios items
        public static final String CONTENT_LIST_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_NOTES;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_NOTES;
    }
}
