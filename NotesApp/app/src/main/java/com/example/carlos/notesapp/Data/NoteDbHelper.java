package com.example.carlos.notesapp.Data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.carlos.notesapp.Data.NotesCointract.NotesEntry;

public class NoteDbHelper extends SQLiteOpenHelper{
    private static String DATABASE_NAME="shelter.db";
    private static final int DATABASE_VERSION=1;

    public NoteDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(NotesEntry.SQL_CREATE_NOTES_TABLET);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
