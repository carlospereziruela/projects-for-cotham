package com.lol.carlos.roomquestionssample;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.util.Log;
import java.util.List;

@Dao
public interface QuestionDao {

  @Insert(onConflict = OnConflictStrategy.IGNORE)
  void insert(QuestionClass question);

  @Query("DELETE FROM question_table")
  void deleteAll();

  @Query("SELECT * from question_table ORDER BY question ASC")
  LiveData<List<QuestionClass>> getAllQuestions();

  //Modify method
  //@Query("UPDATE question_table SET question = :question, answer = :answer WHERE id =:id")
  //void update(String question, String answer, int id);
  @Update
  void update(QuestionClass... question);

}
