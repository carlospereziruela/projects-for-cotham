package com.lol.carlos.roomquestionssample;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import java.util.List;

public class QuestionViewModel extends AndroidViewModel {

  private QuestionRepository mRepository;
  private LiveData<List<QuestionClass>> mAllQuestions;

  public QuestionViewModel(Application application) {
    super(application);
    mRepository = new QuestionRepository(application);
    mAllQuestions = mRepository.getAllQuestions();
  }

  LiveData<List<QuestionClass>> getAllQuestions() {
    return mAllQuestions;
  }

  public void insert(QuestionClass question) {
    mRepository.insert(question);
  }

  public void update(QuestionClass question) {
    mRepository.update(question);
  }
}
