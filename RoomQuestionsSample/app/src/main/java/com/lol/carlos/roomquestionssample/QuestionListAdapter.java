package com.lol.carlos.roomquestionssample;

import static com.lol.carlos.roomquestionssample.MainActivity.MODIFY_ACTIVITY_REQUEST_CODE;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;
import org.w3c.dom.Text;

public class QuestionListAdapter  extends RecyclerView.Adapter<QuestionListAdapter.QuestionViewHolder>{

  private final LayoutInflater mInflater;
  private List<QuestionClass> mQuestions; // Cached copy of questions
  Context context;

  QuestionListAdapter(Context context) {
    mInflater = LayoutInflater.from(context);
    this.context = context;
  }

  @Override
  public QuestionViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
    View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);

    final TextView answerHide = (TextView) itemView.findViewById(R.id.textViewAnswer);
    final ImageButton button = (ImageButton)itemView.findViewById(R.id.button);
    button.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (answerHide.getVisibility() == View.GONE) {
          answerHide.setVisibility(View.VISIBLE);
          button.setImageResource(R.drawable.ic_arrow_upward_black_24dp);
        }else{
          answerHide.setVisibility(View.GONE);
          button.setImageResource(R.drawable.ic_arrow_downward_black_24dp);

        }
      }
    });
    return new QuestionViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(QuestionViewHolder holder, int position) {
    if (mQuestions != null) {
      QuestionClass current = mQuestions.get(position);
      holder.questionItemView.setText(String.valueOf(current.getId()).concat(". " + current.getQuestion()));
      holder.answerItemView.setText(current.getAnswer());
    } else {
      // Covers the case of data not being ready yet.
      holder.questionItemView.setText("No questions");
      holder.answerItemView.setText("No answers");
    }
  }

  void setQuestion(List<QuestionClass> questions){
    mQuestions = questions;
    notifyDataSetChanged();
  }

  // getItemCount() is called many times, and when it is first called,
  // mQuestions has not been updated (means initially, it's null, and we can't return null).
  @Override
  public int getItemCount() {
    if (mQuestions != null)
      return mQuestions.size();
    else return 0;
  }

  class QuestionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    private final TextView questionItemView;
    private final TextView answerItemView;

    private QuestionViewHolder(View itemView) {
      super(itemView);
      questionItemView = itemView.findViewById(R.id.textViewQuestion);
      answerItemView = itemView.findViewById(R.id.textViewAnswer);
      itemView.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
      int mPosition = getLayoutPosition();
      Intent intent = new Intent(context, UpdateQuestion.class);
      intent.putExtra("pereza", mPosition);
      ((Activity) context).startActivityForResult(intent, MODIFY_ACTIVITY_REQUEST_CODE);
      //context.startActivity(intent);
    }
  }
}
