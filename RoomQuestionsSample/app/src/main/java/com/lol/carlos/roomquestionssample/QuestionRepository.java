package com.lol.carlos.roomquestionssample;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;
import java.util.List;

public class QuestionRepository {

  private QuestionDao mQuestionDao;
  private LiveData<List<QuestionClass>> mAllQuestions;

  QuestionRepository(Application application){
    QuestionRoomDatabase db = QuestionRoomDatabase.getDatabase(application);
    mQuestionDao = db.QuestionDao();
    mAllQuestions = mQuestionDao.getAllQuestions();
  }

  LiveData<List<QuestionClass>> getAllQuestions() {
    return mAllQuestions;
  }

  //==================Insert==================
  public void insert (QuestionClass question) {
    new insertAsyncTask(mQuestionDao).execute(question);
  }

  private static class insertAsyncTask extends AsyncTask<QuestionClass, Void, Void> {

    private QuestionDao mAsyncTaskDao;

    insertAsyncTask(QuestionDao dao) {
      mAsyncTaskDao = dao;
    }

    @Override
    protected Void doInBackground(final QuestionClass... params) {
      mAsyncTaskDao.insert(params[0]);
      return null;
    }
  }

  //==================Update==================
  public void update (QuestionClass question) {
    new updateAsyncTask(mQuestionDao).execute(question);
  }

  private static class updateAsyncTask extends AsyncTask<QuestionClass, Void, Void> {

    private QuestionDao mAsyncTaskDao;

    updateAsyncTask(QuestionDao dao) {
      mAsyncTaskDao = dao;
    }

    @Override
    protected Void doInBackground(final QuestionClass... params) {
      //Log.d("0000000000000000000000:", String.valueOf(params[0].getId()));
      //Log.d("0000000000000000000000:", params[0].getQuestion());
      //mAsyncTaskDao.update(params[0].getQuestion(),params[0].getAnswer(), params[0].getId());
      mAsyncTaskDao.update(params[0]);
      return null;
    }
  }

}
