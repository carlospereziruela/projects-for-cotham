package com.lol.carlos.roomquestionssample;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "question_table")
public class QuestionClass {

  @PrimaryKey(autoGenerate = true)
  @ColumnInfo(name = "id")
  private int id;

  @NonNull
  @ColumnInfo(name = "question")
  private String question;

  @ColumnInfo(name = "answer")
  private String answer;

  public QuestionClass(int id, @NonNull String question, String answer) {
    this.id = id;
    this.question = question;
    this.answer = answer;
  }

  public int getId() {
    return id;
  }

  @NonNull
  public String getQuestion() {
    return question;
  }

  public String getAnswer() {
    return answer;
  }
}
