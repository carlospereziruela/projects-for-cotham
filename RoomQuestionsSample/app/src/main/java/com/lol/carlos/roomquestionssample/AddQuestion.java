package com.lol.carlos.roomquestionssample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddQuestion extends AppCompatActivity {

  public static final String EXTRA_REPLY_QUESTION = "com.lol.carlos.roomquetionssample.QUESTION";
  public static final String EXTRA_REPLY_ANSWER = "com.lol.carlos.roomquetionssample.ANSWER";
  private EditText mEditQuestionView;
  private EditText mEditAnswerView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_add_question);

    mEditQuestionView = findViewById(R.id.edit_question);
    mEditAnswerView = findViewById(R.id.edit_answer);
    final Button button = findViewById(R.id.button_save);

    button.setOnClickListener(new View.OnClickListener() {
      public void onClick(View view) {
        Intent replyIntent = new Intent();

        if (TextUtils.isEmpty(mEditQuestionView.getText())) {
          setResult(RESULT_CANCELED, replyIntent);
        } else {
          String question = mEditQuestionView.getText().toString();
          replyIntent.putExtra(EXTRA_REPLY_QUESTION, question);

          String answer;
          if (TextUtils.isEmpty(mEditAnswerView.getText())){
            answer = "No se sae";
          }else{
            answer = mEditAnswerView.getText().toString();
          }
          replyIntent.putExtra(EXTRA_REPLY_ANSWER, answer);

          setResult(RESULT_OK, replyIntent);
        }
        finish();
      }});
  }
}
