package com.lol.carlos.roomquestionssample;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.List;

public class UpdateQuestion extends AppCompatActivity {

  private QuestionViewModel mQuestionViewModel;
  public static final String EXTRA_UPDATE_QUESTION = "com.lol.carlos.roomquetionssample.UQUESTION";
  public static final String EXTRA_UPDATE_ANSWER = "com.lol.carlos.roomquetionssample.UANSWER";
  public static final String EXTRA_UPDATE_ID = "com.lol.carlos.roomquetionssample.UID";
  private EditText mEditQuestionView;
  private EditText mEditAnswerView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_update_question);

    Intent intent = getIntent();
    final int position = intent.getIntExtra("pereza", 0);

    mEditQuestionView = findViewById(R.id.edit_question);
    mEditAnswerView = findViewById(R.id.edit_answer);
    final Button button = findViewById(R.id.button_save);

    button.setOnClickListener(new View.OnClickListener() {
      public void onClick(View view) {
        Intent replyIntent = new Intent();

        if (TextUtils.isEmpty(mEditQuestionView.getText())) {
          setResult(RESULT_CANCELED, replyIntent);
        } else {
          String question = mEditQuestionView.getText().toString();
          replyIntent.putExtra(EXTRA_UPDATE_QUESTION, question);

          String answer;
          if (TextUtils.isEmpty(mEditAnswerView.getText())){
            answer = "No se sae";
          }else{
            answer = mEditAnswerView.getText().toString();
          }
          replyIntent.putExtra(EXTRA_UPDATE_ANSWER, answer);
          replyIntent.putExtra(EXTRA_UPDATE_ID, mQuestionViewModel.getAllQuestions().getValue().get(position).getId());
         // mQuestionViewModel.getAllQuestions().getValue().get(position).getId();

          setResult(RESULT_OK, replyIntent);
        }
        finish();
      }});



    mQuestionViewModel = ViewModelProviders.of(this).get(QuestionViewModel.class);
    mQuestionViewModel.getAllQuestions().observe(this, new Observer<List<QuestionClass>>() {
      @Override
      public void onChanged(@Nullable final List<QuestionClass> questions) {
        // Update the cached copy of the words in the adapter.
        mEditQuestionView.setText(questions.get(position).getQuestion());
        mEditAnswerView.setText(questions.get(position).getAnswer());
      }
    });
  }
}
