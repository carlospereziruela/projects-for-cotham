package com.lol.carlos.roomquestionssample;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

@Database(entities = {QuestionClass.class}, version = 2, exportSchema = false)
public abstract class QuestionRoomDatabase extends RoomDatabase {

  public abstract QuestionDao QuestionDao();

  private static QuestionRoomDatabase INSTANCE;

  public static QuestionRoomDatabase getDatabase(final Context context) {
    if (INSTANCE == null) {
      synchronized (QuestionRoomDatabase.class) {
        if (INSTANCE == null) {
          INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
              QuestionRoomDatabase.class, "question_database")
              .fallbackToDestructiveMigration()
              .addCallback(sRoomDatabaseCallback)
              .build();
        }
      }
    }
    return INSTANCE;
  }

  private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback(){
        @Override
        public void onOpen (@NonNull SupportSQLiteDatabase db){
          super.onOpen(db);
          new PopulateDbAsync(INSTANCE).execute();
        }
      };

  private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

    private final QuestionDao mDao;
    String[] questions = {"Que es el Sol?", "Que es la Luna?", "Que es tu madre?"};
    String[] answers = {"Una esfera gigante de helio en continua fusion nuclear", "No se sae", "No se sae"};


    PopulateDbAsync(QuestionRoomDatabase db) {
      mDao = db.QuestionDao();
    }

    @Override
    protected Void doInBackground(final Void... params) {
      // Start the app with a clean database every time.
      // Not needed if you only populate the database
      // when it is first created
      mDao.deleteAll();

      for (int i = 0; i <= questions.length - 1; i++) {
        QuestionClass word = new QuestionClass(0, questions[i], answers[i]);
        mDao.insert(word);
      }
      return null;
    }
  }
}
