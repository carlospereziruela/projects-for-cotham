package com.lol.carlos.roomquestionssample;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import java.util.List;

public class MainActivity extends AppCompatActivity {

  private QuestionViewModel mQuestionViewModel;
  public static final int NEW_QUESTION_ACTIVITY_REQUEST_CODE = 1;
  public static final int MODIFY_ACTIVITY_REQUEST_CODE = 2;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    FloatingActionButton fab = findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(MainActivity.this, AddQuestion.class);
        startActivityForResult(intent, NEW_QUESTION_ACTIVITY_REQUEST_CODE);
      }
    });

    RecyclerView recyclerView = findViewById(R.id.recyclerview);
    final QuestionListAdapter adapter = new QuestionListAdapter(this);
    recyclerView.setAdapter(adapter);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));

    mQuestionViewModel = ViewModelProviders.of(this).get(QuestionViewModel.class);
    mQuestionViewModel.getAllQuestions().observe(this, new Observer<List<QuestionClass>>() {
      @Override
      public void onChanged(@Nullable final List<QuestionClass> questions) {
        // Update the cached copy of the words in the adapter.
        adapter.setQuestion(questions);
      }
    });

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == NEW_QUESTION_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
      QuestionClass question = new QuestionClass(0,
          data.getStringExtra(AddQuestion.EXTRA_REPLY_QUESTION),
          data.getStringExtra(AddQuestion.EXTRA_REPLY_ANSWER));
      mQuestionViewModel.insert(question);
    }else if(requestCode == MODIFY_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK){
      QuestionClass question = new QuestionClass(data.getIntExtra(UpdateQuestion.EXTRA_UPDATE_ID, 0),
          data.getStringExtra(UpdateQuestion.EXTRA_UPDATE_QUESTION),
          data.getStringExtra(UpdateQuestion.EXTRA_UPDATE_ANSWER));
      mQuestionViewModel.update(question);
    } else {
      Toast.makeText(getApplicationContext(), R.string.empty_not_saved, Toast.LENGTH_LONG).show();
    }
  }
}
