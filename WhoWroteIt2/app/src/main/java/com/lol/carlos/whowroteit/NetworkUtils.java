package com.lol.carlos.whowroteit;


import android.net.Uri;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

public class NetworkUtils {

  private static final String LOG_TAG = NetworkUtils.class.getSimpleName();

  // Base URL for Books API.
  private static final String BOOK_BASE_URL = "https://www.googleapis.com/books/v1/volumes?";
  // Parameter for the search string.
  private static final String QUERY_PARAM = "q";
  // Parameter that limits search results.
  private static final String MAX_RESULTS = "maxResults";
  // Parameter to filter by print type.
  private static final String PRINT_TYPE = "printType";


  static String getBookInfo(String queryString) {

    URL requestURL = createUrl(queryString); // 1
    String bookJSONString = makeHttpRequest(requestURL); // 2 & 3

    Log.d(LOG_TAG, bookJSONString);

    return bookJSONString;
  }

  //============================================== 1 ====================================================
  private static URL createUrl(String queryString) {
    URL requestURL = null;
    try {
      Uri builtURI = Uri.parse(BOOK_BASE_URL).buildUpon()
          .appendQueryParameter(QUERY_PARAM, queryString)
          .appendQueryParameter(MAX_RESULTS, "10")
          .appendQueryParameter(PRINT_TYPE, "books")
          .build();

      requestURL = new URL(builtURI.toString());
    } catch (MalformedURLException e) {
      Log.e(LOG_TAG, "Error creating URL ", e);
    }
    return requestURL;
  }

//================================================ 2 ==================================================
  private static String makeHttpRequest(URL url){
    String jsonResponse = "";

    if (url == null) {
      return jsonResponse;
    }

    HttpURLConnection urlConnection = null;
    InputStream inputStream = null;
    try {
      urlConnection = (HttpURLConnection) url.openConnection();
      urlConnection.setRequestMethod("GET");
      urlConnection.connect();

      if (urlConnection.getResponseCode() == 200) {
        inputStream = urlConnection.getInputStream();
        jsonResponse = readFromStream(inputStream); // 3
      } else {
        Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
      }
    } catch (IOException e) {
      Log.e(LOG_TAG, "Problem retrieving the earthquake JSON results.", e);
    } finally {
      if (urlConnection != null) {
        urlConnection.disconnect();
      }
      if (inputStream != null) {
        try {
          inputStream.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }

    return jsonResponse;
  }

  //============================================= 3 =====================================================
  private static String readFromStream(InputStream inputStream) throws IOException {
    StringBuilder output = new StringBuilder();
    if (inputStream != null) {
      InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
      BufferedReader reader = new BufferedReader(inputStreamReader);
      String line;

      while ((line = reader.readLine()) != null) {
        output.append(line);
        //output.append("/n");
      }
    }
    return output.toString();
  }

}
