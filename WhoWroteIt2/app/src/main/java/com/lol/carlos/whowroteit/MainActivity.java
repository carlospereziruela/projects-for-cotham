package com.lol.carlos.whowroteit;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {


  private EditText mBookInput;
  private TextView mTitleText;
  private TextView mAuthorText;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    mBookInput = (EditText)findViewById(R.id.bookInput);
    mTitleText = (TextView)findViewById(R.id.titleText);
    mAuthorText = (TextView)findViewById(R.id.authorText);


  }

  public void searchBooks(View view) {
    // Get the search string from the input field.
    String queryString = mBookInput.getText().toString();

    //Code that hides the keyboard
    InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    if (inputManager != null ) {
      inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    //Network state
    ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo networkInfo = null;
    if (connMgr != null) {
      networkInfo = connMgr.getActiveNetworkInfo();
    }
    if (networkInfo != null && networkInfo.isConnected() && queryString.length() != 0) {
      //Get Request Process
      new FetchBook(mTitleText, mAuthorText).execute(queryString);
      mAuthorText.setText("");
      mTitleText.setText(R.string.loading);
    } else {
      if (queryString.length() == 0) {
        //No query
        mAuthorText.setText("");
        mTitleText.setText(R.string.no_search_term);
      } else {
        //No Internet
        mAuthorText.setText("");
        mTitleText.setText(R.string.no_network);
      }
    }

  }

  public class FetchBook extends AsyncTask<String, Void, String[]>{
    //TextViews we'll need to modify
    private WeakReference<TextView> mTitleText;
    private WeakReference<TextView> mAuthorText;

    FetchBook(TextView titleText, TextView authorText) {
      this.mTitleText = new WeakReference<>(titleText);
      this.mAuthorText = new WeakReference<>(authorText);
    }

    //Calling the class “NetworkUtils” and passing the results to “extractAutor…”
    @Override
    protected String[] doInBackground(String... strings) {
      return extractAutorTitleFromJsonBook(NetworkUtils.getBookInfo(strings[0]));
    }

    //Checking if the data is valid to show “No results”
    @Override
    protected void onPostExecute(String[] datos){
      super.onPostExecute(datos);

      if (datos[0] != null || datos[1] != null) {
        mTitleText.get().setText(datos[0]);
        mAuthorText.get().setText(datos[1]);
      }else{
        mTitleText.get().setText("No results");
        mAuthorText.get().setText("");
      }
      Toast.makeText(getApplicationContext(), "Finish", Toast.LENGTH_SHORT).show();
    }
  }

  private static String[] extractAutorTitleFromJsonBook(String JSON) {
    if (TextUtils.isEmpty(JSON)) {
      return null;
    }
    String[] datos = new String[2];
    try {
      JSONObject jsonObject = new JSONObject(JSON);
      JSONArray itemsArray = jsonObject.getJSONArray("items");

      int i = 0;
      String title = null;
      String authors = null;

      while (i < itemsArray.length() && (authors == null && title == null)) {
        JSONObject book = itemsArray.getJSONObject(i);
        JSONObject volumeInfo = book.getJSONObject("volumeInfo");

        title = volumeInfo.getString("title");
        authors = volumeInfo.getString("authors");

        i++;
      }
      datos[0] = title;
      datos[1] = authors;
    } catch (JSONException e) {
      datos[0] = "No results";
      datos[1] = "";
    }
    return datos;
  }
}
