package com.lol.carlos.roomwordssample;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import java.util.List;

public class WordRepository {

  private WordDao mWordDao;
  private LiveData<List<WordClass>> mAllWords;

  WordRepository(Application application) {
    WordRoomDatabase db = WordRoomDatabase.getDatabase(application);
    mWordDao = db.wordDao();
    mAllWords = mWordDao.getAllWords();
  }

  //Devuelve las palabras en caché como LiveData
  LiveData<List<WordClass>> getAllWords() {
    return mAllWords;
  }

  public void insert (WordClass word) {
    new insertAsyncTask(mWordDao).execute(word);
  }

  private static class insertAsyncTask extends AsyncTask<WordClass, Void, Void> {

    private WordDao mAsyncTaskDao;

    insertAsyncTask(WordDao dao) {
      mAsyncTaskDao = dao;
    }

    @Override
    protected Void doInBackground(final WordClass... params) {
      mAsyncTaskDao.insert(params[0]);
      return null;
    }
  }

  //====================Delete all=========================
  public void deleteAll () {
    new deleteAllAsyncTask(mWordDao).execute();
  }

  private static class deleteAllAsyncTask extends AsyncTask<Void, Void, Void> {

    private WordDao mAsyncTaskDao;

    deleteAllAsyncTask(WordDao dao) {
      mAsyncTaskDao = dao;
    }

    @Override
    protected Void doInBackground(Void... params) {
      mAsyncTaskDao.deleteAll();
      return null;
    }
  }

  //====================Delete one====================
  public void deleteWord(WordClass word)  {
    new deleteWordAsyncTask(mWordDao).execute(word);
  }

  private static class deleteWordAsyncTask extends AsyncTask<WordClass, Void, Void> {
    private WordDao mAsyncTaskDao;

    deleteWordAsyncTask(WordDao dao) {
      mAsyncTaskDao = dao;
    }

    @Override
    protected Void doInBackground(final WordClass... params) {
      mAsyncTaskDao.deleteWord(params[0]);
      return null;
    }
  }

}
