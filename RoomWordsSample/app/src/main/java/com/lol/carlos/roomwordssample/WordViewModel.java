package com.lol.carlos.roomwordssample;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import java.util.List;

public class WordViewModel extends AndroidViewModel {

  private WordRepository mRepository;
  private LiveData<List<WordClass>> mAllWords;

  public WordViewModel(Application application) {
    super(application);
    mRepository = new WordRepository(application);
    mAllWords = mRepository.getAllWords();
  }

  LiveData<List<WordClass>> getAllWords() {
    return mAllWords;
  }

  public void insert(WordClass word) {
    mRepository.insert(word);
  }

  public void deleteAll(){
    mRepository.deleteAll();
  }

  public void deleteWord(WordClass word) {
    mRepository.deleteWord(word);
  }
}
