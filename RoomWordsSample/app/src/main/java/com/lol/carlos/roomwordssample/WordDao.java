package com.lol.carlos.roomwordssample;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import java.util.List;

@Dao
public interface WordDao {

  @Insert(onConflict = OnConflictStrategy.IGNORE)
  void insert(WordClass word);

  @Query("DELETE FROM word_table")
  void deleteAll();

  @Query("SELECT * from word_table ORDER BY word ASC")
  LiveData<List<WordClass>> getAllWords();

  @Query("SELECT * from word_table LIMIT 1")
  WordClass[] getAnyWord();

  @Delete
  void deleteWord(WordClass word);
}
