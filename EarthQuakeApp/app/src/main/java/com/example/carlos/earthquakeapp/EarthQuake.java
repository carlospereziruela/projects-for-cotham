package com.example.carlos.earthquakeapp;

/**
 * Created by Carlos on 27/03/2018.
 */

public class EarthQuake {
    private double magnitud;
    private String lugar;
    private String fecha;
    private String url;

    public EarthQuake(String magnitud, String lugar, String fecha, String url) {
        this.magnitud = Double.parseDouble(magnitud);
        this.lugar = lugar;
        this.fecha = fecha;
        this.url = url;
    }

    public double getMagnitud() {
        return magnitud;
    }

    public void setMagnitud(double magnitud) {
        this.magnitud = magnitud;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
