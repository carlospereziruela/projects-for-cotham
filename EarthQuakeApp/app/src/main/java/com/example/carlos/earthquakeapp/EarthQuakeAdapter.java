package com.example.carlos.earthquakeapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Carlos on 27/03/2018.
 */

public class EarthQuakeAdapter extends ArrayAdapter<EarthQuake>{
    //private int idEarthQuake;

    public EarthQuakeAdapter(Activity context, ArrayList<EarthQuake> earthQuakes) {
        super(context, 0, earthQuakes);
        //this.idEarthQuake=idEarthQuake;
    }
    public int magnitudColor(Double magnitud){
        int magnitudeColorResourceId;
        int magnitudeFloor = (int) (magnitud-(magnitud%magnitud));
        switch (magnitudeFloor) {
            case 0:
            case 1:
                magnitudeColorResourceId = R.color.magnitude1;
                break;
            case 2:
                magnitudeColorResourceId = R.color.magnitude2;
                break;
            case 3:
                magnitudeColorResourceId = R.color.magnitude3;
                break;
            case 4:
                magnitudeColorResourceId = R.color.magnitude4;
                break;
            case 5:
                magnitudeColorResourceId = R.color.magnitude5;
                break;
            case 6:
                magnitudeColorResourceId = R.color.magnitude6;
                break;
            case 7:
                magnitudeColorResourceId = R.color.magnitude7;
                break;
            case 8:
                magnitudeColorResourceId = R.color.magnitude8;
                break;
            case 9:
                magnitudeColorResourceId = R.color.magnitude9;
                break;
            default:
                magnitudeColorResourceId = R.color.magnitude10plus;
                break;
        }
        return ContextCompat.getColor(getContext(), magnitudeColorResourceId);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.earthquake_view, parent, false);
        }
        EarthQuake currentWord = getItem(position);
        //-------------------MAGNITUD---------------------------
        TextView magnitud = (TextView) convertView.findViewById(R.id.magnitud);
        magnitud.setText(String.valueOf(currentWord.getMagnitud()));

        GradientDrawable magnitudeCircle = (GradientDrawable) magnitud.getBackground();
        int magnitudeColor = magnitudColor(currentWord.getMagnitud());
        magnitudeCircle.setColor(magnitudeColor);

        //magnitud.setBackgroundColor(magnitudColor(currentWord.getMagnitud()));

        //-------------------LUGAR---------------------------
        TextView lugar = (TextView) convertView.findViewById(R.id.lugar);
        TextView lugar2 = (TextView) convertView.findViewById(R.id.lugar2);

        String originalLocation = currentWord.getLugar();
        String LOCATION_SEPARATOR = " of ";
        String primaryLocation="";
        String locationOffset="";
        if (originalLocation.contains(LOCATION_SEPARATOR)) {
            String[] parts = originalLocation.split(LOCATION_SEPARATOR);
            locationOffset = parts[0] + LOCATION_SEPARATOR;
            primaryLocation = parts[1];
        } else {
            locationOffset = "Near of ";
            primaryLocation = originalLocation;
        }

        lugar.setText(locationOffset);
        lugar2.setText(primaryLocation);
        //-------------------FECHA---------------------------
        TextView fecha = (TextView) convertView.findViewById(R.id.fecha);
        TextView time=(TextView) convertView.findViewById(R.id.time);

        long timeInMilliseconds = Long.parseLong(currentWord.getFecha());
        Date dateObject = new Date(timeInMilliseconds);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM, dd, yyyy");
        SimpleDateFormat dateFormatter2 = new SimpleDateFormat("h:mm a");
        String dateToDisplay = dateFormatter.format(dateObject);
        String timeToDisplay= dateFormatter2.format(dateObject);
        //String dateToDisplay = dateFormatter.format(dateObject);
        fecha.setText(dateToDisplay);
        time.setText(timeToDisplay);

        return convertView;
    }

    //----------------------------------------------Nice Methods------------------------------------------------
    /*
    private String formatMagnitude(double magnitude) {
        DecimalFormat magnitudeFormat = new DecimalFormat("0.0");
        return magnitudeFormat.format(magnitude);
    }

    private String formatDate(Date dateObject) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("LLL dd, yyyy");
        return dateFormat.format(dateObject);
    }

    private String formatTime(Date dateObject) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
        return timeFormat.format(dateObject);
    }
    */
}
