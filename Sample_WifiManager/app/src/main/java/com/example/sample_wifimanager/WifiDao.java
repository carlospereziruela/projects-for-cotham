package com.example.sample_wifimanager;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import java.util.List;

@Dao
public interface WifiDao {

  @Insert(onConflict = OnConflictStrategy.IGNORE)
  void insert(WifiClass wifiObject);

  @Insert(onConflict = OnConflictStrategy.IGNORE)
  void insertAll(List<WifiClass> wifiList);

  @Query("DELETE FROM wifi_table")
  void deleteAll();

  @Query("SELECT * from wifi_table ORDER BY SSID ASC")
  LiveData<List<WifiClass>> getAllWifi();

  @Query("SELECT * from wifi_table LIMIT 1")
  WifiClass[] getAnyWifi();

}
