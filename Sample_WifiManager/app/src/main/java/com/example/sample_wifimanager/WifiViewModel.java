package com.example.sample_wifimanager;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;
import java.util.List;

// Responsible for holding and processing all the data needed for the UI as requested in the tasks
public class WifiViewModel extends AndroidViewModel {

  private WifiRepository wifiRepository;
  private LiveData<List<WifiClass>> allWifi;

  public WifiViewModel(@NonNull Application application) {
    super(application);
    wifiRepository = new WifiRepository(application);
    allWifi = wifiRepository.getAllWifi();
  }

  LiveData<List<WifiClass>> getAllWifi() {
    return allWifi;
  }

  public void insert(WifiClass wifiObject) {
    wifiRepository.insert(wifiObject);
  }
  public void deleteAll(){
    wifiRepository.deleteAll();
  }

}
