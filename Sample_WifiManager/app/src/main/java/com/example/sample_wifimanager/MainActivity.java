package com.example.sample_wifimanager;

import android.Manifest;
import android.Manifest.permission;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

  private WifiViewModel wifiViewModel;
  WifiListAdapter adapter;

  List<ScanResult> results;
  WifiManager wifiManager;
  LinearLayout linearView;
  Button start_stop;

  public static final int PERMISSIONS_REQUEST_LOCATION_21 = 99;
  public static final int PERMISSIONS_REQUEST_LOCATION_22 = 100;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    start_stop = findViewById(R.id.start_stop);


//    linearView = findViewById(R.id.linearView);
//
//    wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
//
//    if (!wifiManager.isWifiEnabled()){
//      Toast.makeText(this, "Wifi disable... Activating...", Toast.LENGTH_SHORT).show();
//      wifiManager.setWifiEnabled(true);
//    }
//
//
//    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
//      requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 0x12345);
//      //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
//
//    }else{
//      registerReceiver(wifiReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
//      wifiManager.startScan();
//      Toast.makeText(this, "Scanning...", Toast.LENGTH_SHORT).show();
//      //do something, permission was previously granted; or legacy device
//    }

    //=====================================
    RecyclerView recyclerView = findViewById(R.id.recyclerview);
    adapter = new WifiListAdapter(this);
    recyclerView.setAdapter(adapter);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));

    //The app can't returns an empty value on "scan"
    if(VERSION.SDK_INT >= VERSION_CODES.M && checkSelfPermission(permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
      requestPermissions(new String[]{permission.ACCESS_COARSE_LOCATION}, PERMISSIONS_REQUEST_LOCATION_22);

//    }else if(VERSION.SDK_INT < VERSION_CODES.M &&
//        ContextCompat.checkSelfPermission(this,
//            Manifest.permission.ACCESS_FINE_LOCATION)
//            != PackageManager.PERMISSION_GRANTED){
//
//        new Builder(this).setTitle(R.string.title_location_permission).setMessage(R.string.text_location_permission)
//            .setPositiveButton(R.string.ok, new OnClickListener() {
//              @Override
//              public void onClick(DialogInterface dialogInterface, int i) {
//                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_LOCATION_21);
//              }})
//            .setNegativeButton(R.string.no, new OnClickListener() {
//              @Override
//              public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(getApplicationContext(), "Without location permission the app doesn't work", Toast.LENGTH_LONG).show();
//              }
//            })
//            .create().show();

    }else{
      wifiViewModel = ViewModelProviders.of(this).get(WifiViewModel.class);
     // wifiViewModel.deleteAll();
      wifiViewModel.getAllWifi().observe(this, new Observer<List<WifiClass>>() {
        @Override
        public void onChanged(@Nullable final List<WifiClass> wifiList) {
          adapter.setAllWifi(wifiList);
        }
      });
    }



  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    if (requestCode == PERMISSIONS_REQUEST_LOCATION_21){
//      if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//        if (ContextCompat.checkSelfPermission(this,
//            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//
//          wifiViewModel = ViewModelProviders.of(this).get(WifiViewModel.class);
//          wifiViewModel.getAllWifi().observe(this, new Observer<List<WifiClass>>() {
//            @Override
//            public void onChanged(@Nullable final List<WifiClass> wifiList) {
//              adapter.setAllWifi(wifiList);
//            }
//          });
//
//        }
//      }
//    }else{
      if (grantResults[0] == PackageManager.PERMISSION_DENIED){
        Toast.makeText(this, "Without location permission the app doesn't work", Toast.LENGTH_LONG).show();
      }else{
        wifiViewModel = ViewModelProviders.of(this).get(WifiViewModel.class);
        wifiViewModel.getAllWifi().observe(this, new Observer<List<WifiClass>>() {
          @Override
          public void onChanged(@Nullable final List<WifiClass> wifiList) {
            // Update the cached copy of the words in the adapter.
            adapter.setAllWifi(wifiList);
          }
        });
      }
   // }
  }

  /*BroadcastReceiver wifiReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      results = wifiManager.getScanResults();
      unregisterReceiver(this);

      for (ScanResult scanResult : results) {
        TextView textView = new TextView(context);
        textView.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        textView.setTextColor(getResources().getColor(R.color.white));
        textView.setText(scanResult.toString());
        linearView.addView(textView);
        LayoutParams lp = (LinearLayout.LayoutParams) textView.getLayoutParams();
        lp.setMargins(16,16, 16, 16);
        textView.setLayoutParams(lp);
        textView.setPadding(16,16,16,16);

      }
    }


  };*/


  public void start_scan(View view) {
    Intent intent = new Intent(this, Wifi_Service.class);
    if (start_stop.getText().toString().equalsIgnoreCase("Start")) {
      startService(intent);
      start_stop.setText("Stop");
    }else{
      stopService(intent);
      start_stop.setText("Start");

    }
  }
}
