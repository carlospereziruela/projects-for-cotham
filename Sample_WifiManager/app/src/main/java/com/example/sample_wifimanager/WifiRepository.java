package com.example.sample_wifimanager;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class WifiRepository {

  private WifiDao wifiDao;
  private LiveData<List<WifiClass>> allWifi;

  WifiRepository(Application application) {
    WifiRoomDatabase db = WifiRoomDatabase.getDatabase(application);
    wifiDao = db.wifiDao();
    allWifi = wifiDao.getAllWifi();
  }

  // Get all Wifi objects
  LiveData<List<WifiClass>> getAllWifi() {
    return allWifi;
  }

  //Insert one Wifi object
  public void insert (WifiClass wifiObject){
    new insertAsyncTask(wifiDao).execute(wifiObject);
  }

  private static class insertAsyncTask extends AsyncTask<WifiClass, Void, Void> {

    private WifiDao mAsyncTaskDao;

    insertAsyncTask(WifiDao wifiDao) {
      mAsyncTaskDao = wifiDao;
    }

    @Override
    protected Void doInBackground(final WifiClass... params) {
      mAsyncTaskDao.insert(params[0]);
      return null;
    }
  }
 //Delete All objects
  public void deleteAll (){
    new deleteAsyncTask(wifiDao).execute();
  }

  private static class deleteAsyncTask extends AsyncTask<Void, Void, Void> {

    private WifiDao mAsyncTaskDao;

    deleteAsyncTask(WifiDao wifiDao) {
      mAsyncTaskDao = wifiDao;
    }

    @Override
    protected Void doInBackground(final Void... params) {
      mAsyncTaskDao.deleteAll();
      return null;
    }
  }


}
