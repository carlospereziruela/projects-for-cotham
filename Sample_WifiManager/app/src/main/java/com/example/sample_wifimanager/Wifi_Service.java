package com.example.sample_wifimanager;

import android.app.IntentService;
import android.arch.lifecycle.LifecycleService;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Wifi_Service extends IntentService {

  private static Context classContext;

  static WifiRoomDatabase db;
  //private static WifiViewModel uibi;
  static List<ScanResult> results;
  private static WifiManager wifiManager;
  //private LiveData<List<WifiClass>> mAllWifi;

  Handler handler = new Handler();
  private Runnable periodicUpdate = new Runnable() {
    @Override
    public void run() {
      if (android.os.Build.VERSION.SDK_INT == VERSION_CODES.O || android.os.Build.VERSION.SDK_INT == VERSION_CODES.O) {
        handler.postDelayed(periodicUpdate, 1000*60*30);
      }else if(android.os.Build.VERSION.SDK_INT >= VERSION_CODES.P){
        handler.postDelayed(periodicUpdate, 1000*30);
      }else{
        handler.postDelayed(periodicUpdate, 1000*15);
      }
      new PopulateDbAsync().execute();

    }

    @Override
    protected void finalize() throws Throwable {
      super.finalize();
    }
  };


  public Wifi_Service(/*WifiRoomDatabase db*/) {
    super("wifi_service");
  }

  @Override
  protected void onHandleIntent(Intent intent) {

  }

  //Populate the database in the background.
  private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

    PopulateDbAsync() {
    }

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      wifiManager = (WifiManager) classContext.getSystemService(WIFI_SERVICE);

      if (!wifiManager.isWifiEnabled()){
        Toast.makeText(classContext, "Wifi disable... Activating...", Toast.LENGTH_SHORT).show();
        wifiManager.setWifiEnabled(true);
      }

      results = wifiManager.getScanResults();
      wifiManager.startScan();
      Toast.makeText(classContext, "Scanning...", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected Void doInBackground(Void... voids) {
      db.wifiDao().deleteAll();
      for (ScanResult scanResult : results) {
        String ssid = scanResult.SSID;
        if (ssid == null || ssid.isEmpty()){
          ssid = "Hide SSID";
        }
        String bssid = scanResult.BSSID;
        String capabilities = scanResult.capabilities;
        int level = scanResult.level;
        int frequency = scanResult.frequency;
        long timestamp = scanResult.timestamp;

        int passpoint; int channelWidth; int centerFreq0; int centerFreq1; int responder;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
          if (scanResult.isPasspointNetwork() == true) {
            passpoint = 1;
          }else{
            passpoint = 0;
          }
          channelWidth = scanResult.channelWidth;
          centerFreq0 = scanResult.centerFreq0;
          centerFreq1 = scanResult.centerFreq1;
          if (scanResult.is80211mcResponder() == true){
            responder = 1;
          }else{
            responder = 0;
          }
        }else{
          passpoint = 0;
          channelWidth = 0;
          centerFreq0 = 0;
          centerFreq1 = 0;
          responder = 0;

        }

        WifiClass wifiObject = new WifiClass(ssid, bssid, capabilities, level, frequency,
            timestamp, passpoint, channelWidth, centerFreq0, centerFreq1, responder);
        db.wifiDao().insert(wifiObject);

      }

      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);
      Toast.makeText(classContext, "Scanning finished", Toast.LENGTH_SHORT).show();

    }
  }

  @Override
  public void onStart(Intent intent, int startId) {
    super.onStart(intent, startId);
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    Toast.makeText(this, "Scanning...", Toast.LENGTH_SHORT).show();

    Bundle extras = intent.getExtras();
    if(extras == null) {
      Log.d("Service", "null");
    }else {
      Log.d("Service","not null");
    }
    classContext = getApplicationContext();

    db = WifiRoomDatabase.getDatabase(getApplicationContext());

    handler.post(periodicUpdate);
    return START_STICKY;


    //return super.onStartCommand(intent, flags, startId);
  }

  @Override
  public void onDestroy() {
    Toast.makeText(this, "Destroy scanning service", Toast.LENGTH_SHORT).show();
    handler.removeCallbacks(periodicUpdate);
    super.onDestroy();
  }
}
