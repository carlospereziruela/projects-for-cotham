package com.example.sample_wifimanager;

import static android.content.Context.WIFI_SERVICE;

import android.Manifest;
import android.app.Activity;
import android.app.IntentService;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

@Database(entities = {WifiClass.class}, version = 1, exportSchema = false)
public abstract class WifiRoomDatabase extends RoomDatabase implements Serializable{

  public abstract WifiDao wifiDao();
  private static WifiRoomDatabase INSTANCE;
  public static Context context;

  static List<ScanResult> results;
  private static WifiManager wifiManager;

  public static WifiRoomDatabase getDatabase(final Context contextt) {
    context = contextt;
    if (INSTANCE == null) {
      synchronized (WifiRoomDatabase.class) {
        if (INSTANCE == null) {
          INSTANCE = Room.databaseBuilder(context.getApplicationContext(), WifiRoomDatabase.class, "wifi_database")
              .fallbackToDestructiveMigration() // Wipes and rebuilds
              .addCallback(sRoomDatabaseCallback) //
              .build();
        }
      }
    }
    return INSTANCE;
  }

  private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback(){
    @Override
    public void onOpen (@NonNull SupportSQLiteDatabase db){
      super.onOpen(db);
      new PopulateDbAsync(INSTANCE).execute();

    }
  };

  //Populate the database in the background.
  private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {
    private final WifiDao mDao;

    PopulateDbAsync(WifiRoomDatabase db) {
      mDao = db.wifiDao();
    }

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      wifiManager = (WifiManager) context.getSystemService(WIFI_SERVICE);

      if (!wifiManager.isWifiEnabled()){
        wifiManager.setWifiEnabled(true);
      }

        results = wifiManager.getScanResults();
        wifiManager.startScan();
    }

    @Override
    protected Void doInBackground(Void... voids) {

      //The app can't returns an empty value on "getScanResults" when using a lower version than Android 6.0.
      //In theory, until Android 5.1, it's not necessary to request realtime permissions but it doesn't work in lower versions
      for (ScanResult scanResult : results) {
          String ssid = scanResult.SSID;
          if (ssid == null || ssid.isEmpty()){
            ssid = "Hide SSID";
          }
          String bssid = scanResult.BSSID;
          String capabilities = scanResult.capabilities;
          int level = scanResult.level;
          int frequency = scanResult.frequency;
          long timestamp = scanResult.timestamp;

        int passpoint; int channelWidth; int centerFreq0; int centerFreq1; int responder;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
          if (scanResult.isPasspointNetwork() == true) {
            passpoint = 1;
          }else{
            passpoint = 0;
          }
          channelWidth = scanResult.channelWidth;
          centerFreq0 = scanResult.centerFreq0;
          centerFreq1 = scanResult.centerFreq1;
          if (scanResult.is80211mcResponder() == true){
            responder = 1;
          }else{
            responder = 0;
          }
        }else{
          passpoint = 0;
          channelWidth = 0;
          centerFreq0 = 0;
          centerFreq1 = 0;
          responder = 0;

        }

          WifiClass wifiObject = new WifiClass(ssid, bssid, capabilities, level, frequency,
              timestamp, passpoint, channelWidth, centerFreq0, centerFreq1, responder);
          mDao.insert(wifiObject);
        //storeLogData(wifiObject);
      }

      return null;
    }
  }
  public static void storeLogData(WifiClass wifiObject){
    File logFile = Environment.getExternalStorageDirectory();
    logFile = new File(logFile.getPath() + "/Android/data/com.example.sample_wifimanager/files");
    if(!logFile.exists()){
      logFile.mkdirs();
    }

    FileOutputStream fos;
    try {
      fos = new FileOutputStream(logFile.getPath() + "/log_wifi_file.txt");

      fos.write(wifiObject.toString().getBytes());
      fos.write("\n".getBytes());
      fos.close();

    } catch (FileNotFoundException e) {
      e.printStackTrace();
      Log.d(WifiRepository.class.getSimpleName(), "File Access Error");

    } catch (IOException e) {
      e.printStackTrace();
      Log.d(WifiRepository.class.getSimpleName(), "File Access Error");
    }
  }
}
