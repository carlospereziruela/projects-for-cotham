package com.example.sample_wifimanager;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;

public class WifiListAdapter extends RecyclerView.Adapter<WifiListAdapter.WifiViewHolder> {

  private final LayoutInflater mInflater;
  private List<WifiClass> allWifi; // Cached wifi list

  public WifiListAdapter(Context context) {
    mInflater = LayoutInflater.from(context);
  }

  @Override
  public WifiViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
    return new WifiViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(WifiViewHolder holder, int position) {
    WifiClass wifiObject = allWifi.get(position);
    holder.ssidTextView.setText(wifiObject.getSSID());
    holder.bssidTextView.setText(wifiObject.getBSSID());
    holder.capabilitiesTextView.setText(wifiObject.getCapabilities());
    holder.levelTextView.setText(String.valueOf(wifiObject.getLevel()));
    holder.frequencyTextView.setText(String.valueOf(wifiObject.getFrequency()));
    holder.timeStampTextView.setText(String.valueOf(wifiObject.getTimeStamp()));
    if (wifiObject.getPasspoint() == 1){
      holder.passpointTextView.setText("Yes");
    }else{
      holder.passpointTextView.setText("No");
    }
    holder.channelWidthTextView.setText(String.valueOf(wifiObject.getChannelWidth()));
    holder.centerFreq_0_TextView.setText(String.valueOf(wifiObject.getCenterFreq_0()));
    holder.centerFreq_1_TextView.setText(String.valueOf(wifiObject.getCenterFreq_1()));
    if (wifiObject.getMcResponder() == 1) {
      holder.mcResponderTextView.setText("Yes");
    }else{
      holder.mcResponderTextView.setText("No");

    }

  }

  void setAllWifi(List<WifiClass> allWifi){
    this.allWifi = allWifi;
    notifyDataSetChanged();
  }

  @Override
  public int getItemCount() {
    if (allWifi != null){
      return  allWifi.size();
    }else{
      return 0;
    }
  }

  class WifiViewHolder extends RecyclerView.ViewHolder {
    private final TextView ssidTextView;
    private final TextView bssidTextView;
    private final TextView capabilitiesTextView;
    private final TextView levelTextView;
    private final TextView frequencyTextView;
    private final TextView timeStampTextView;
    private final TextView passpointTextView;
    private final TextView channelWidthTextView;
    private final TextView centerFreq_0_TextView;
    private final TextView centerFreq_1_TextView;
    private final TextView mcResponderTextView;

    private final LinearLayout hideExtraData;


    private WifiViewHolder(View itemView) {
      super(itemView);
      ssidTextView = itemView.findViewById(R.id.ssid);
      bssidTextView = itemView.findViewById(R.id.bssid);
      capabilitiesTextView = itemView.findViewById(R.id.capabilities);
      levelTextView = itemView.findViewById(R.id.level);
      frequencyTextView = itemView.findViewById(R.id.frequency);
      timeStampTextView = itemView.findViewById(R.id.timeStamp);
      passpointTextView = itemView.findViewById(R.id.passpoint);
      channelWidthTextView = itemView.findViewById(R.id.channelWidth);
      centerFreq_0_TextView = itemView.findViewById(R.id.centerFreq0);
      centerFreq_1_TextView = itemView.findViewById(R.id.centerFreq1);
      mcResponderTextView = itemView.findViewById(R.id.responder);

      hideExtraData = itemView.findViewById(R.id.hideExtraData);
      itemView.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          if (hideExtraData.getVisibility() == View.VISIBLE){
            hideExtraData.setVisibility(View.GONE);
          }else{
            hideExtraData.setVisibility(View.VISIBLE);
          }
        }
      });
    }
  }

  public WifiClass getWifiFromPosition(int position){
    return allWifi.get(position);
  }
}
