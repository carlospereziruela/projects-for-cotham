package com.example.sample_wifimanager;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "wifi_table")
public class WifiClass {

  @PrimaryKey
  @NonNull
  @ColumnInfo(name = "SSID")
  private String SSID;

  @ColumnInfo(name = "BSSID")
  private String BSSID;

  @ColumnInfo(name = "CAPABILITIES")
  private String capabilities;
  @ColumnInfo(name = "LEVEL")
  private int level;
  @ColumnInfo(name = "FREQUENCY")
  private int frequency;
  @ColumnInfo(name = "TIMESTAMP")
  private long timeStamp;
  @ColumnInfo(name = "PASSPOINT")
  private int passpoint;
  @ColumnInfo(name = "CHANNELWIDTH")
  private int channelWidth;
  @ColumnInfo(name = "CENTERFREQ_0")
  private int centerFreq_0;
  @ColumnInfo(name = "CENTERFREQ_1")
  private int centerFreq_1;
  @ColumnInfo(name = "MCRESPONDER")
  private int mcResponder;

  public WifiClass(@NonNull String SSID, String BSSID, String capabilities, int level, int frequency,
      long timeStamp, int passpoint, int channelWidth, int centerFreq_0, int centerFreq_1, int mcResponder) {

    this.SSID = SSID;
    this.BSSID = BSSID;
    this.capabilities = capabilities;
    this.level = level;
    this.frequency = frequency;
    this.timeStamp = timeStamp;
    this.passpoint = passpoint;
    this.channelWidth = channelWidth;
    this.centerFreq_0 = centerFreq_0;
    this.centerFreq_1 = centerFreq_1;
    this.mcResponder = mcResponder;
  }


  @NonNull
  public String getSSID() {
    return SSID;
  }

  public String getBSSID() {
    return BSSID;
  }

  public String getCapabilities() {
    return capabilities;
  }

  public int getLevel() {
    return level;
  }

  public int getFrequency() {
    return frequency;
  }

  public long getTimeStamp() {
    return timeStamp;
  }

  public int getPasspoint() {
    return passpoint;
  }

  public int getChannelWidth() {
    return channelWidth;
  }

  public int getCenterFreq_0() {
    return centerFreq_0;
  }

  public int getCenterFreq_1() {
    return centerFreq_1;
  }

  public int getMcResponder() {
    return mcResponder;
  }
}
